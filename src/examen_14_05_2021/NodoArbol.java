/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_14_05_2021;

/**
 *
 * @author Jose
 */
class NodoArbol {
 int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol(int d, String a){
        this.dato = d;
        this.nombre = a;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
        public String toString(){
            return nombre + "El dato es: "+dato;
        }
}

