/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_14_05_2021;

/**
 *
 * @author Jose
 */
public class Examen_14_05_2021 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        ArbolBinario Arbolito = new ArbolBinario();
        Arbolito.agregarNodo(74, "J");
        Arbolito.agregarNodo(79, "O");
        Arbolito.agregarNodo(83, "S");
        Arbolito.agregarNodo(69, "E");
        Arbolito.agregarNodo(77, "M");
        Arbolito.agregarNodo(65, "A");
        Arbolito.agregarNodo(82, "R");
        Arbolito.agregarNodo(73, "I");
        Arbolito.agregarNodo(78, "N");
        Arbolito.agregarNodo(68, "D");
        Arbolito.agregarNodo(85, "U");
        Arbolito.agregarNodo(67, "C");
        Arbolito.agregarNodo(89, "Y");
        
        
        System.out.println("InOrden");
        if(!Arbolito.estaVacio()){
           Arbolito.inOrden(Arbolito.raiz);
        }
System.out.println("PreOrden");
        if(!Arbolito.estaVacio()){
           Arbolito.preOrden(Arbolito.raiz);
        }
System.out.println("PosOrden");
        if(!Arbolito.estaVacio()){
           Arbolito.posOrden(Arbolito.raiz);
        }        
    }
    
}
